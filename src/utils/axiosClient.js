import axios from 'axios';

const axiosClient = axios.create({
  baseURL: 'http://122.170.1.83:8899/api/v1',
  timeout: 300000,
  header: {
    'Content-Type': 'multipart/form-data'
  }
});

export default axiosClient;